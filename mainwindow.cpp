#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QString>
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    connect(ui->pushButtonOpen,SIGNAL(clicked()),this,SLOT(openFile()));
}

MainWindow::~MainWindow()
{delete ui;}
void MainWindow::openFile()
{
    ui->textBrowser->clear();
    ui->textBrowser_2->clear();
    QString fileName = ui->lineEdit->text();

    if(!DH.openFile(fileName))
    {
        ui->textBrowser->insertPlainText("NO FILE");
        return;
    }


