#ifndef DH_H
#define DH_H
#include <QFile>
#include <QMap>
#include <QList>
#include <QPair>
#include <QString>
#include <QStringList>

//"������� ���� �������, � ������� �� ��, ��� �� �� �������� �� ������ ����� �����..."

enum TypeofLink
{ISA,PARTOF,CONTAINS,DOESTO};

class Link
{
public:
    Link();
    int start;
    int end;
    TypeofLink typeoflink;
    QString point;
    //QString act;
    Link(int start, int end, TypeofLink typeoflink, QString position,QString _act):
        start(start),end(end),typeoflink(typeoflink),point(position)/*, act(_act) */{}
    ~Link();

//"...������ � ��� ���� ������..."

    int StartEntity() const {return start;}
    int EndEntity() const {return end;}
    TypeofLink type()const {return typeoflink;}
    QString position(){return point;}
    //QString action(){return act;}
    QString Nameoftype(){
        switch (typeoflink){
        case ISA: return "is a";
        case PARTOF: return "is part of";
        case CONTAINS: return "contains";
        }
    }
};
class DH
{
    QFile file;
    QMap<int,QString> entities;
    QList<Link*> links;
public:
    DH();
    ~DH();
    QStringList AllLinks();
    QStringList AllNewLinks();
    QStringList AllEntities();
    bool OpenFile(QString name);
    void CreateLinks();
    bool UsefullData();
private:
    void AddEntity(int, QString);
    void AddEntity(QString);
    void AddLink(Link*);
    void AddLink(int, int, TypeofLink, QString point/*, QString act = ""*/);
    QList<Link*> FindAllLinks(TypeofLink) const;
    bool Subsist(Link*);
};
#endif // DH_H
